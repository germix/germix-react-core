import React from 'react';

interface Props
{
    label,
    checked,
    disabled?,
    onClick(e),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class CheckMenuItem extends React.Component<Props,State>
{
    mounted = false;

    render()
    {
        let className = ['menu-item'];
        if(this.props.disabled)
            className.push('disabled');
        return (
<div className={className.join(' ')} onClick={(e) =>
{
    if(this.props.onClick && !this.props.disabled)
        this.props.onClick(e);
}} >
    <div className="checkbox mr-2">
        <input
            type="checkbox"
            checked={this.props.checked}
            disabled={this.props.disabled}
            onChange={(e) =>
            {
            }}
        />
        <span className="checkbox-icon"></span>
    </div>
    {this.props.label}
</div>
        );
    }

    componentDidMount()
    {
        this.mounted = true;
    }

    componentWillUnmount()
    {
        this.mounted = false;
    }
}
export default CheckMenuItem;
