import React from 'react';

interface Props
{
    disabled;
    onClick();
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class PaginationItem extends React.Component<Props,State>
{
    render()
    {
        let className = ['page-item'];

        if(this.props.disabled)
            className.push('disabled');
        
        return (
<li className={className.join(' ')}>
    <a className="page-link" href="#" onClick={ (event) =>
        {
            event.preventDefault();
            if(!this.props.disabled)
            {
                this.props.onClick();
            }
        }
    }>
        <span className="sr-only">
            { this.props.children }
        </span>
    </a>
</li>
        )
    }
};
export default PaginationItem;
