import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Breadcrumbs extends React.Component<Props,State>
{
    render()
    {
        return (
<ol className="breadcrumbs">
    {
        React.Children.toArray(this.props.children).map((child, index) =>
        {
            return <li key={index}>{child}</li>
        })
    }
</ol>
        );
    }
};
export default Breadcrumbs;
