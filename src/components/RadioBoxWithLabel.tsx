import React, { PropsWithChildren } from 'react';
import RadioBox from './RadioBox';

interface Props extends PropsWithChildren<any>
{
    name?,
    value?,
    selected?,
    disabled? : boolean,
    onChange?(value),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class RadioBoxWithLabel extends React.Component<Props,State>
{
    render()
    {
        return (
<label className='radiobox-with-label'>
    <RadioBox
        name={this.props.name}
        value={this.props.value}
        selected={this.props.selected}
        disabled={this.props.disabled}
        onChange={this.props.onChange}
    >
    </RadioBox>
    { this.props.children }
</label>
        );
    }
};
export default RadioBoxWithLabel;
