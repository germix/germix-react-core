import React, { PropsWithChildren } from 'react';
import ReactDOM from 'react-dom';

interface Props extends PropsWithChildren<any>
{
    open? : boolean,
    disableEscapeKeyDown? : boolean,
    disableBackdropClick? : boolean,
    onClose?(),
    onEscapeKeyDown?(event),
    onBackdropClick?(event),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Modal extends React.Component<Props,State>
{
    mouseDownTarget;
    render()
    {
        if(!this.props.open)
        {
            return null;
        }
        return ReactDOM.createPortal(
            <div className={'modal'}
                onClick={this.onClick}
                onKeyDown={this.handleKeyDown}
                onMouseDown={this.handleMouseDown}
            >
                { this.props.children }
            </div>
        ,
        document.body);
    }

    onClick = (event) =>
    {
        event.stopPropagation();

        // Ignore the events not coming from the "backdrop"
        // We don't want to close the dialog when clicking the dialog content
        if(event.target !== event.currentTarget)
        {
            return;
        }

        // Make sure the event starts and ends on the same DOM element
        if(event.target !== this.mouseDownTarget)
        {
            return;
        }

        this.mouseDownTarget = null;

        if(this.props.onBackdropClick)
        {
            this.props.onBackdropClick(event);
        }
        if(!this.props.disableBackdropClick && this.props.onClose)
        {
            this.props.onClose();
        }
    }

    handleKeyDown = (event) =>
    {
        if(event.key !== 'Escape')
        {
            return;
        }
        event.stopPropagation();

        if(this.props.onEscapeKeyDown)
        {
            this.props.onEscapeKeyDown(event);
        }

        if(!this.props.disableEscapeKeyDown && this.props.onClose)
        {
            this.props.onClose();
        }
    }

    handleMouseDown = (event) =>
    {
        this.mouseDownTarget = event.target;
    }
};
export default Modal;
