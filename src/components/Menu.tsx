import React, { PropsWithChildren } from 'react';
import Popup from './Popup';

interface Props extends PropsWithChildren<any>
{
    arrow?,
    anchor?,
    horz_policy?,
    vert_policy?,
    closeable?,
    onClose?(event?),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Menu extends React.Component<Props,State>
{
    static defaultProps =
    {
        open: false,
        closeable: true,
    };
    render()
    {
        return (
<Popup
    arrow={this.props.arrow}
    anchor={this.props.anchor}
    horz_policy={this.props.horz_policy}
    vert_policy={this.props.vert_policy}
    onClose={this.props.onClose}
    
    closeable={this.props.closeable}
>
    <div className="menu-popup-content">
        { this.props.children }
    </div>
</Popup>
        );
    }
};
export default Menu;
