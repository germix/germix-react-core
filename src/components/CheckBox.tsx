import React from 'react';

interface Props
{
    name? : string,
    checked? : boolean,
    disabled? : boolean,
    onChange?(checked, name),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class CheckBox extends React.Component<Props,State>
{
    static defaultProps =
    {
        name: '',
    };
    render()
    {
        let className = ['checkbox'];
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        return (
<div className={className.join(' ')}>
    <label>
        <input
            type="checkbox"
            checked={this.props.checked}
            disabled={this.props.disabled}
            onChange={ (e) =>
            {
                if(this.props.onChange)
                    this.props.onChange(e.target.checked, this.props.name);
            }}
        />
        <span className="checkbox-icon"></span>
    </label>
</div>
        );
    }
};
export default CheckBox;
