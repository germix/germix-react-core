import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class InputGroupAppend extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="input-group-append">
    { this.props.children }
</div>
        );
    }
};
export default InputGroupAppend;
