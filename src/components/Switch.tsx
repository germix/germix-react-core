import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
    checked? : boolean,
    disabled? : boolean,
    onChange?(checked: boolean),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Switch extends React.Component<Props,State>
{
    constructor(props)
    {
        super(props);
    }
    render()
    {
        let className = ['switch'];
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        return (
<div className={className.join(' ')} >
    <label>
        <input
            type="checkbox"
            checked={this.props.checked}
            disabled={this.props.disabled}
            onChange={ (e) =>
        {
            if(this.props.onChange)
            {
                this.props.onChange(!this.props.checked);
            }
        }}/>
        <span className="switch-icon"></span>
        <span className="switch-label">{this.props.children}</span>
    </label>
</div>
        );
    }
};
export default Switch;
