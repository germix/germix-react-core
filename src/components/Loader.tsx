import React from 'react';
import { ColorTypes } from '../types';

interface Props
{
    size? : 'sm',
    type?,
    color? : ColorTypes,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Loader extends React.Component<Props,State>
{
    render()
    {
        let type = (this.props.type || 'quart');
        let className = ['loader'];

        className.push('loader-' + type);

        if(this.props.size !== undefined)
            className.push('loader-' + type + '-' + this.props.size);

        if(this.props.color !== undefined)
            className.push('loader-'+this.props.color);

        return (
<span className={className.join(' ')}></span>
        );
    }
};
export default Loader;
