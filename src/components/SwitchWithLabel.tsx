import React, { PropsWithChildren } from 'react';
import Switch from './Switch';

interface Props extends PropsWithChildren<any>
{
    checked? : boolean,
    disabled? : boolean,
    onChange?(checked: boolean),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class SwitchWithLabel extends React.Component<Props,State>
{
    render()
    {
        return (
<label className='switch-with-label'>
    <Switch
        checked={this.props.checked}
        disabled={this.props.disabled}
        onChange={this.props.onChange}
    >
    </Switch>
    { this.props.children }
</label>
        );
    }
};
export default SwitchWithLabel;
