import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
    size? : 'sm'|'lg',
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class InputGroup extends React.Component<Props,State>
{
    render()
    {
        let className = ['input-group'];
        if(this.props.size !== undefined)
        {
            className.push('input-group-' + this.props.size);
        }
        return (
<div className={className.join(' ')}>
    { this.props.children }
</div>
        );
    }
};
export default InputGroup;
