import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class DialogFooter extends React.Component<Props,State>
{
    render()
    {
        return (
            <div className="dialog-footer">
                { this.props.children }
            </div>
        );
    }
};
export default DialogFooter;
