import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
    right?,
    center?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class ButtonToolbar extends React.Component<Props,State>
{
    render()
    {
        return (
<div className={"button-toolbar"
    + (this.props.right ? ' align-right' : '')
    + (this.props.center ? ' align-center' : '')
}>
    { this.props.children }
</div>
        );
    }
};
export default ButtonToolbar;
