import React from 'react';

interface Props
{
    min? : number,
    max? : number,
    value? : number,
    step? : number,
    disabled? : boolean,
    vertical? : boolean,
    inverted? : boolean,
    onChange?(newValue),
    onChangeCommited?(newValue),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Slider extends React.Component<Props,State>
{
    state =
    {
        pressed: false
    }
    sliderRef;
    static defaultProps =
    {
        min : 0,
        max : 100,
        value : 0,
        step: 1,
        vertical: undefined,
        inverted: undefined,
    };

    render()
    {
        let percent = valueToPercent(this.props.min, this.props.max, this.props.value);

        const trackStyle =
        {
            width: percent + '%'
        }
        const thumbStyle =
        {
            left: percent + '%'
        }

        let className = ['slider'];
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        return (
<div
    ref={(e) => this.sliderRef = e}
    className={className.join(' ')}
    onMouseDown={this.handleMouseDown}
    >
    <div className="slider-wrapper">
        <input type="hidden"></input>
        <div className="slider-rail">
        </div>
        <div className="slider-track" style={trackStyle}>
        </div>
        <div
            className={"slider-thumb" + (this.state.pressed ? ' active' : '')}
            style={thumbStyle}
            onBlur={this.handleBlur}
            onFocus={this.handleFocus}
            onKeyDown={this.handleKeyDown}
        >
        </div>
    </div>
</div>
        );
    }

    handleBlur = (event) =>
    {
    }

    handleFocus = (event) =>
    {
    }

    handleKeyDown = (event) =>
    {
    }

    handleMouseDown = (event) =>
    {
        if(this.props.disabled)
        {
            return;
        }
        event.preventDefault();

        this.setState({
            pressed: true
        });

        this.sliderRef.focus();

        const finger = this.trackFinder(event);
        const {
            newValue
        } = this.getFingerNewValue(finger);

        if(this.props.onChange)
        {
            this.props.onChange(newValue);
        }

        document.body.addEventListener('mouseup', this.handleMouseUp);
        document.body.addEventListener('mousemove', this.handleMouseMove);
    }

    handleMouseUp = (event) =>
    {
        this.setState({
            pressed: false
        });
        const finger = this.trackFinder(event);

        if(!finger)
            return;

        const {
            newValue
        } = this.getFingerNewValue(finger);

        if(this.props.onChangeCommited)
        {
            this.props.onChangeCommited(newValue);
        }

        document.body.removeEventListener('mouseup', this.handleMouseUp);
        document.body.removeEventListener('mousemove', this.handleMouseMove);
    }

    handleMouseMove = (event) =>
    {
        const finger = this.trackFinder(event);

        if(!finger)
            return;

        const {
            newValue
        } = this.getFingerNewValue(finger);

        if(this.props.onChange)
        {
            this.props.onChange(newValue);
        }
    }

    trackFinder = (event) =>
    {
        return {
            x: event.clientX,
            y: event.clientY,
        };
    }

    getFingerNewValue = (finger) =>
    {
        let percent;
        const { width, height, bottom, left} = this.sliderRef.getBoundingClientRect();

        if(this.props.vertical)
            percent = (bottom - finger.y) / height;
        else
            percent = (finger.x - left) / width;

        if(this.props.inverted)
            percent = 1 - percent;

        let newValue = percentToValue(this.props.min, this.props.max, percent);

        if(this.props.step)
        {
            newValue = roundValueToStep(newValue, this.props.step);
        }

        newValue = clamp(this.props.min, this.props.max, newValue);

        return {
            newValue,
        };
    }
};
export default Slider;

function clamp(min, max, value)
{
    if(value < min) return min;
    if(value > max) return max;
    return value;
  }
function valueToPercent(min, max, value)
{
    return ((value - min) * 100) / (max - min);
}
function percentToValue(min, max, percent)
{
    return (max - min) * percent + min;
}
function getDecimalPrecision(num)
{
    // This handles the case when num is very small (0.00000001), js will turn this into 1e-8.
    // When num is bigger than 1 or less than -1 it won't get converted to this notation so it's fine.
    if(Math.abs(num) < 1)
    {
        const parts = num.toExponential().split('e-');
        const matissaDecimalPart = parts[0].split('.')[1];
        return (matissaDecimalPart ? matissaDecimalPart.length : 0) + parseInt(parts[1], 10);
    }

    const decimalPart = num.toString().split('.')[1];
    return decimalPart ? decimalPart.length : 0;
}
function roundValueToStep(value, step)
{
    const nearest = Math.round(value / step) * step;
    return Number(nearest.toFixed(getDecimalPrecision(step)));
}
