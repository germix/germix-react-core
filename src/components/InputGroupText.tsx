import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class InputGroupText extends React.Component<Props,State>
{
    render()
    {
        return (
<span className="input-group-text">
    { this.props.children }
</span>
        );
    }
};
export default InputGroupText;
