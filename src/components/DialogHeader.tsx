import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
    title,
    onClose?(),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class DialogHeader extends React.Component<Props,State>
{
    render()
    {
        return (
            <div className="dialog-header">
                <div className="dialog-header-title">{this.props.title}</div>
                { this.props.onClose &&
                    <div className="dialog-header-close" onClick={ () =>
                    {
                        if(this.props.onClose)
                            this.props.onClose();
                    }}>X</div>
                }
            </div>
        );
    }
};
export default DialogHeader;
