import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class DialogBody extends React.Component<Props,State>
{
    render()
    {
        return (
            <div className="dialog-body">
                { this.props.children }
            </div>
        );
    }
}
export default DialogBody;
