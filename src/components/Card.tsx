import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Card extends React.Component<Props,State>
{
    render()
    {
        return (
            <div className="card">
                <div className="card-body">
                    { this.props.children}
                </div>
            </div>
        );
    }
};
export default Card;
