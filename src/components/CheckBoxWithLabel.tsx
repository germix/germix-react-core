import React, { PropsWithChildren } from 'react';
import CheckBox from './CheckBox';

interface Props extends PropsWithChildren<any>
{
    name? : string,
    checked? : boolean,
    disabled? : boolean,
    onChange(checked, name),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class CheckBoxWithLabel extends React.Component<Props,State>
{
    render()
    {
        return (
<label className='checkbox-with-label'>
    <CheckBox
        name={this.props.name}
        checked={this.props.checked}
        disabled={this.props.disabled}
        onChange={this.props.onChange}
    >
    </CheckBox>
    { this.props.children }
</label>
        );
    }
};
export default CheckBoxWithLabel;
