import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
    className?;
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class FormGroup extends React.Component<Props,State>
{
    render()
    {
        let className = 'form-group';
        if(this.props.className)
        {
            className += ' ' + this.props.className;
        }
        return (
<div className={className} style={this.props.style}>
    {this.props.children}
</div>
        );
    }
};
export default FormGroup;
