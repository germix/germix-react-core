import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class InputGroupPrepend extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="input-group-prepend">
    { this.props.children }
</div>
        );
    }
};
export default InputGroupPrepend;
