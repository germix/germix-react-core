import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
    name?,
    value?,
    selected?,
    disabled? : boolean,
    onChange?(value),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class RadioBox extends React.Component<Props,State>
{
    static defaultProps =
    {
        name: '',
        value: '',
        selected: undefined,
        disabled: undefined,
    };
    render()
    {
        let className = ['radiobox'];
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        return (
<div className={className.join(' ')}>
    <label>
        <input
            type="radio"
            name={this.props.name}
            checked={this.props.selected === this.props.value}
            disabled={this.props.disabled}
            onChange={ (e) =>
            {
                if(this.props.onChange)
                    this.props.onChange(this.props.value);
            }}
        />
        <span className="radiobox-icon"></span>
    </label>
</div>
        );
    }
};
export default RadioBox;
