import React from 'react';
import PaginationItem from './PaginationItem';

interface Props
{
    disabled?,
    neighbors?,
    totalRecords?,
    pageLimit?,
    currentPage?,
    right?,

    firstLabel?,
    previousLabel?,
    nextLabel?,
    lastLabel?,

    onPageChanged(newPage: number),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Pagination extends React.Component<Props,State>
{

    static defaultProps =
    {
        neighbors: 3,
    };
    render()
    {
        let startPage: number;
        let endPage: number;

        const neighbors = this.props.neighbors;

        // Calculate total pages
        const totalPages = Math.ceil(this.props.totalRecords / this.props.pageLimit);

        // Current page
        const currentPage = this.props.currentPage;

        if(totalPages <= neighbors*2+1)
        {
            startPage = 1;
            endPage = totalPages;
        }
        else
        {
            const neighbors = this.props.neighbors;

            startPage = currentPage - neighbors;
            endPage = currentPage + neighbors;
            if(startPage <= 1)
            {
                endPage -= (startPage - 1);
                startPage = 1;
                endPage = neighbors*2 + 1;
            }
            if(endPage > totalPages)
            {
                endPage = totalPages;
                startPage = totalPages - neighbors*2;
            }
        }

        let pages: number[] = [];
        for(let i = startPage; i <= endPage; i++)
        {
            pages.push(i);
        }

        return (
<ul className="pagination" style={{ justifyContent: this.props.right ? 'flex-end' : 'flex-start' }}>

    { this.props.firstLabel &&
        <PaginationItem disabled={ this.props.disabled || currentPage === 1 } onClick={() => this.gotoPage(1)} >
            {this.props.firstLabel}
        </PaginationItem>
    }

    { this.props.previousLabel &&
        <PaginationItem disabled={ this.props.disabled || currentPage === 1 } onClick={() => this.gotoPage(this.props.currentPage - 1)} >
            {this.props.previousLabel}
        </PaginationItem>
    }

    { pages.map((page, index) =>
        <PaginationItem key={index} disabled={ this.props.disabled || currentPage === page } onClick={() => this.gotoPage(page)} >
            { page }
        </PaginationItem>
    )}

    { this.props.nextLabel &&
        <PaginationItem disabled={ this.props.disabled || totalPages == 0 || currentPage === totalPages } onClick={() => this.gotoPage(this.props.currentPage + 1)} >
            {this.props.nextLabel}
        </PaginationItem>
    }
    
    { this.props.lastLabel &&
        <PaginationItem disabled={ this.props.disabled || totalPages == 0 || currentPage === totalPages } onClick={() => this.gotoPage(totalPages)} >
            {this.props.lastLabel}
        </PaginationItem>
    }

</ul>
        );
    }

    gotoPage = (page) =>
    {
        if(this.props.currentPage !== page)
        {
            const totalPages = Math.ceil(this.props.totalRecords / this.props.pageLimit);

            this.props.onPageChanged(Math.max(1, Math.min(page, totalPages)));
        }
    }
};
export default Pagination;
