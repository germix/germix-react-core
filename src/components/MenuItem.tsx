import React from 'react';

interface Props
{
    icon?,
    image?,
    label?,
    rounded?,
    onClick?(event),
    disabled?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class MenuItem extends React.Component<Props,State>
{
    render()
    {
        let className = ['menu-item'];
        if(this.props.disabled)
            className.push('disabled');
        return (
<div className={className.join(' ')} onClick={(e) => 
{
    if(this.props.onClick && !this.props.disabled)
        this.props.onClick(e);
}} >
    {this.props.icon &&
        <i className={this.props.icon}></i>
    }
    {this.props.image &&
        <img src={this.props.image} className={this.props.rounded ? 'rounded' : ''}/>
    }
    {this.props.label}
</div>
        );
    }
};
export default MenuItem;
