import React from 'react';

interface Props
{
    id?,
    index?,

    href?,
    icon?,
    label?,
    active? : boolean,
    disabled? : boolean,
    onClick(event, tabItem),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class TabItem extends React.Component<Props,State>
{
    render()
    {
        let className = ['tab-item'];
        if(this.props.active)
        {
            className.push('active');
        }
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        return (
<div
    className={className.join(' ')}
    onClick={ (event) =>
    {
        if(!(this.props.disabled))
        {
            event.preventDefault();
            event.stopPropagation();
            this.props.onClick(event, this);
        }
    }}
>
    <a href={this.props.href}>
        {this.props.icon && <i className={this.props.icon} /> }
        {this.props.label}
    </a>
</div>
        );
    }
};
export default TabItem;
