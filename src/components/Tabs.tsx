import React from 'react';
import TabItem from './TabItem';

interface Props
{
    activeTab,
    fullBorders?,
    onActivateTab?(tab),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Tabs extends React.Component<Props,State>
{
    render()
    {
        const tabsChildren = React.Children.toArray(this.props.children);

        return (
<div className="tabs">
    <div className="tabs-nav">
        {
            tabsChildren.map((child: any, index) =>
            {
                return (
                    <TabItem
                        id={child.props.id}
                        index={index}
                        key={child.props.id}
                        href={child.props.href}
                        icon={child.props.icon}
                        label={child.props.label}
                        active={child.props.id === this.props.activeTab}
                        disabled={child.props.disabled}
                        onClick={ this.handleClickTabItem }
                    ></TabItem>
                );
            })
        }
    </div>
    <div className={"tabs-content" + (this.props.fullBorders ? ' full-borders' : '') }>
    {
        tabsChildren.map((child: any) =>
        {
            return <div key={child.props.id} style={{
                display: child.props.id !== this.props.activeTab ? 'none' : undefined
            }}>
                { child }
            </div>
        })
    }
    </div>
</div>
        );
    }
    handleClickTabItem = (event, tab) =>
    {
        if(this.props.onActivateTab)
            this.props.onActivateTab(tab);
    }
};
export default Tabs;
