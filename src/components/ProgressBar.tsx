import React from 'react';
import { ColorTypes } from '../types';

interface Props
{
    progress,
    color? : ColorTypes,
    striped? : boolean,
    animated? : boolean,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class ProgressBar extends React.Component<Props,State>
{
    render()
    {
        const styles: any =
        {
            '--width': this.props.progress + '%',
        }
        return (
<div
    style={styles}
    className={
        "progress-bar"
        + (this.props.color ? ' progress-bar-'+this.props.color : '')
        + (this.props.striped ? ' progress-bar-striped' : '')
        + (this.props.animated ? ' animated' : '')}
>
</div>
        );
    }
};
export default ProgressBar;
