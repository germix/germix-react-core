import React from 'react';

interface Props
{
    innerRef?,
    type?,
    value?,
    disabled? : boolean,
    required? : boolean,
    readonly? : boolean,
    placeholder? : string,
    onChange?(event),
    onKeyDown?(event),
    onKeyUp?(event),
    onKeyPress?(event),
    extraClasses?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class TextField extends React.Component<Props,State>
{
    static defaultProps =
    {
        type: 'text',
    };
    render()
    {
        let className = ['form-control'];
        if(this.props.extraClasses)
        {
            className.push(...this.props.extraClasses.split(' '));
        }
        return (
            <>
<input
    ref={this.props.innerRef}
    className={className.join(' ')}
    type={this.props.type}
    value={this.props.value}
    disabled={(this.props.disabled ? true : undefined)}
    required={(this.props.required ? true : undefined)}
    readOnly={(this.props.readonly ? true : undefined)}
    placeholder={this.props.placeholder}
    onChange={this.props.onChange}
    onKeyDown={this.props.onKeyDown}
    onKeyUp={this.props.onKeyUp}
    onKeyPress={this.props.onKeyPress}
></input>
            </>
        );
    }
};
export default TextField;
