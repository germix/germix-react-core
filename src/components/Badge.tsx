import React from 'react';
import { ColorTypes } from '../types';

interface Props
{
    href?,
    pill?,
    color? : ColorTypes,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Badge extends React.Component<Props, State>
{
    mounted = false;

    render()
    {
        let className = ['badge'];
        if(this.props.pill)
        {
            className.push('badge-pill');
        }
        if(this.props.color)
        {
            className.push('badge-' + this.props.color);
        }
        if(this.props.href)
        {
            return (
            <a href={this.props.href} className={className.join(' ')}>
                { this.props.children }
            </a>
            );
        }
        return (
        <div className={className.join(' ')}>
            { this.props.children }
        </div>
        );
    }
}
export default Badge;
