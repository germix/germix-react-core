import React, { PropsWithChildren } from 'react';
import { ColorTypes } from '../types';

interface Props extends PropsWithChildren<any>
{
    innerRef?,
    size? : 'lg'|'sm'|'xs',
    color? : 'default'|ColorTypes,
    block?,
    disabled? : boolean,
    link? : boolean,
    outline? : boolean,
    gradient? : boolean,
    tool? : boolean,
    fullwidth? : boolean,
    uppercase? : boolean,
    rounded? : boolean,
    processing? : boolean,
    extraClasses?,
    icon?,
    onClick?(event),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Button extends React.Component<Props,State>
{
    render()
    {
        let className = ['btn'];

        if(this.props.color !== undefined)
        {
            if(this.props.gradient)
                className.push('bg-gradient-' + this.props.color);
            else if(this.props.outline)
                className.push('btn-outline-' + this.props.color);
            else
                className.push('btn-' + this.props.color);
        }
        if(this.props.size !== undefined)
        {
            className.push('btn-' + this.props.size);
        }
        if(this.props.block)
        {
            className.push('btn-block');
        }
        if(this.props.disabled)
        {
            className.push('disabled');
        }
        if(this.props.link)
        {
            className.push('btn-link');
        }
        if(this.props.tool)
        {
            className.push('btn-tool');
        }
        if(this.props.rounded)
        {
            className.push('btn-rounded');
        }
        if(this.props.processing)
        {
            className.push('btn-processing');
        }
        if(this.props.fullwidth)
        {
            className.push('btn-fullwidth');
        }
        if(this.props.extraClasses !== undefined)
        {
            className.push(...this.props.extraClasses.split(' '));
        }
        return (
<button
    ref={this.props.innerRef}
    className={className.join(' ')}
    disabled={this.props.disabled}
    onClick={this.props.onClick}
    type='button'
>
    {this.props.icon && <i className={this.props.icon} /> }
    { this.props.children }
</button>
        );
    }
};
export default Button;
