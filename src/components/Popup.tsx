import React from 'react';

interface Props
{
    arrow?,
    anchor?,
    horz_policy?,
    vert_policy?,
    fullwidth?,
    closeable?,
    onClose?(event?),

}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Popup extends React.Component<Props,State>
{
    static defaultProps =
    {
        open: false,
        arrow: undefined,
        anchor: 'bottom-center',
        horz_policy: 'left',
        vert_policy: undefined,
    };
    state =
    {
        posX: 0,
        posY: 0,
    }
    refPopup: any = null;
    refContent: any = null;

    render()
    {
        let popupClass = 'popup';

        if(this.props.arrow)
        {
            popupClass += ' arrow';
            
            if(this.props.horz_policy !== undefined)
            {
                if(this.props.anchor === 'top-left' && this.props.horz_policy === 'left')           popupClass += ' arrow-bottom-right-corner';
                if(this.props.anchor === 'top-left' && this.props.horz_policy === 'right')          popupClass += ' arrow-bottom-left';
                if(this.props.anchor === 'top-left' && this.props.horz_policy === 'center')         popupClass += ' arrow-bottom-center';

                if(this.props.anchor === 'top-center' && this.props.horz_policy === 'left')         popupClass += ' arrow-bottom-right';
                if(this.props.anchor === 'top-center' && this.props.horz_policy === 'right')        popupClass += ' arrow-bottom-left';
                if(this.props.anchor === 'top-center' && this.props.horz_policy === 'center')       popupClass += ' arrow-bottom-center';

                if(this.props.anchor === 'top-right' && this.props.horz_policy === 'left')          popupClass += ' arrow-bottom-right';
                if(this.props.anchor === 'top-right' && this.props.horz_policy === 'right')         popupClass += ' arrow-bottom-left-corner';
                if(this.props.anchor === 'top-right' && this.props.horz_policy === 'center')        popupClass += ' arrow-bottom-center';

                if(this.props.anchor === 'bottom-left' && this.props.horz_policy === 'left')        popupClass += ' arrow-top-right-corner';
                if(this.props.anchor === 'bottom-left' && this.props.horz_policy === 'right')       popupClass += ' arrow-top-left';
                if(this.props.anchor === 'bottom-left' && this.props.horz_policy === 'center')      popupClass += ' arrow-top-center';

                if(this.props.anchor === 'bottom-center' && this.props.horz_policy === 'left')      popupClass += ' arrow-top-right';
                if(this.props.anchor === 'bottom-center' && this.props.horz_policy === 'right')     popupClass += ' arrow-top-left';
                if(this.props.anchor === 'bottom-center' && this.props.horz_policy === 'center')    popupClass += ' arrow-top-center';

                if(this.props.anchor === 'bottom-right' && this.props.horz_policy === 'left')       popupClass += ' arrow-top-right';
                if(this.props.anchor === 'bottom-right' && this.props.horz_policy === 'right')      popupClass += ' arrow-top-left-corner';
                if(this.props.anchor === 'bottom-right' && this.props.horz_policy === 'center')     popupClass += ' arrow-top-center';
            }
            else if(this.props.vert_policy !== undefined)
            {
                if(this.props.anchor === 'top-left' && this.props.vert_policy === 'up')             popupClass += ' arrow-right-bottom-corner';
                if(this.props.anchor === 'top-left' && this.props.vert_policy === 'down')           popupClass += ' arrow-right-top';
                if(this.props.anchor === 'top-left' && this.props.vert_policy === 'center')         popupClass += ' arrow-right-center';

                if(this.props.anchor === 'top-right' && this.props.vert_policy === 'up')            popupClass += ' arrow-left-bottom-corner';
                if(this.props.anchor === 'top-right' && this.props.vert_policy === 'down')          popupClass += ' arrow-left-top';
                if(this.props.anchor === 'top-right' && this.props.vert_policy === 'center')        popupClass += ' arrow-left-center';

                if(this.props.anchor === 'bottom-left' && this.props.vert_policy === 'up')          popupClass += ' arrow-right-bottom';
                if(this.props.anchor === 'bottom-left' && this.props.vert_policy === 'down')        popupClass += ' arrow-right-top-corner';
                if(this.props.anchor === 'bottom-left' && this.props.vert_policy === 'center')      popupClass += ' arrow-right-center';

                if(this.props.anchor === 'bottom-right' && this.props.vert_policy === 'up')         popupClass += ' arrow-left-bottom';
                if(this.props.anchor === 'bottom-right' && this.props.vert_policy === 'down')       popupClass += ' arrow-left-top-corner';
                if(this.props.anchor === 'bottom-right' && this.props.vert_policy === 'center')     popupClass += ' arrow-left-center';

                if(this.props.anchor === 'center-left' && this.props.vert_policy === 'up')          popupClass += ' arrow-right-bottom';
                if(this.props.anchor === 'center-left' && this.props.vert_policy === 'down')        popupClass += ' arrow-right-top';
                if(this.props.anchor === 'center-left' && this.props.vert_policy === 'center')      popupClass += ' arrow-right-center';

                if(this.props.anchor === 'center-right' && this.props.vert_policy === 'up')         popupClass += ' arrow-left-bottom';
                if(this.props.anchor === 'center-right' && this.props.vert_policy === 'down')       popupClass += ' arrow-left-top';
                if(this.props.anchor === 'center-right' && this.props.vert_policy === 'center')     popupClass += ' arrow-left-center';
            }
        }
        if(this.props.fullwidth)
        {
            popupClass += ' fullwidth';
        }
        const popupStyle =
        {
            left: this.state.posX,
            top: this.state.posY,
        }
        return (
            <div
                ref={(e) => { this.refPopup = e}}
                style={popupStyle}
                className={popupClass}
                onClick={this.handleClick}
            >
                <div
                    ref={(e) => { this.refContent = e}}
                >
                    { this.props.children }
                </div>
            </div>
        );
    }
    componentDidMount()
    {
        document.addEventListener('click', this.handleWindowMouseClick);

        if(this.refContent == null)
            return;

        let posX = 0;
        let posY = 0;
        const pos = { left: 0, top: 0};
        const parentWidth = this.refPopup.offsetParent.offsetWidth;
        const parentHeight = this.refPopup.offsetParent.offsetHeight;
        const contentWidth = this.refContent.offsetWidth;
        const contentHeight = this.refContent.offsetHeight;

        if(this.props.horz_policy !== undefined)
        {
            if(this.props.anchor === 'top-left' || this.props.anchor === 'top-right' || this.props.anchor === 'top-center')
            {
                posY = pos.top - contentHeight;
                if(this.props.arrow)
                    posY -= 8;
            }
            else if(this.props.anchor === 'bottom-left' || this.props.anchor === 'bottom-right' || this.props.anchor === 'bottom-center')
            {
                posY = pos.top + parentHeight;
                if(this.props.arrow)
                    posY += 8;
            }
            else
            {
                console.log("Popup: Invalid anchor for horz_policy");
            }

            if(this.props.anchor === 'top-left' || this.props.anchor === 'bottom-left')
            {
                //if(this.props.horz_policy === 'left')            posX = pos.left - contentWidth - 8;
                if(this.props.horz_policy === 'left')           posX = pos.left - contentWidth;
                else if(this.props.horz_policy === 'right')     posX = pos.left;
                else if(this.props.horz_policy === 'center')    posX = pos.left - contentWidth / 2;
            }
            else if(this.props.anchor === 'top-right' || this.props.anchor === 'bottom-right')
            {
                if(this.props.horz_policy === 'left')           posX = pos.left + parentWidth - contentWidth;
                //else if(this.props.horz_policy === 'right')      posX = pos.left + parentWidth + 8;
                else if(this.props.horz_policy === 'right')     posX = pos.left + parentWidth;
                else if(this.props.horz_policy === 'center')    posX = pos.left + parentWidth - contentWidth / 2;
            }
            else if(this.props.anchor === 'top-center' || this.props.anchor === 'bottom-center')
            {
                if(this.props.horz_policy === 'left')
                {
                    posX = pos.left + parentWidth/2 - contentWidth;
                    posX += 15+1;
                }
                else if(this.props.horz_policy === 'right')
                {
                    posX = pos.left + parentWidth/2;
                    posX -= 15+1;
                }
                else if(this.props.horz_policy === 'center')
                {
                    posX = (pos.left - (contentWidth/2 - parentWidth/2));
                }
            }
        }
        else if(this.props.vert_policy !== undefined)
        {
            if(this.props.anchor === 'top-left' || this.props.anchor === 'bottom-left' || this.props.anchor === 'center-left')
            {
                posX = pos.left - contentWidth;
                if(this.props.arrow)
                    posX -= 8;
            }
            else if(this.props.anchor === 'top-right' || this.props.anchor === 'bottom-right' || this.props.anchor === 'center-right')
            {
                posX = pos.left + parentWidth;
                if(this.props.arrow)
                    posX += 8;
            }
            else
            {
                console.log("Popup: Invalid anchor for vert_policy");
            }

            if(this.props.anchor === 'top-left' || this.props.anchor === 'top-right')
            {
                //if(this.props.vert_policy === 'up')              posY = pos.top - contentHeight - 8;
                if(this.props.vert_policy === 'up')             posY = pos.top - contentHeight;
                else if(this.props.vert_policy === 'down')      posY = pos.top;
                else if(this.props.vert_policy === 'center')    posY = pos.top - contentHeight / 2;
            }
            else if(this.props.anchor === 'bottom-left' || this.props.anchor === 'bottom-right')
            {
                if(this.props.vert_policy === 'up')             posY = pos.top + parentHeight - contentHeight;
                //else if(this.props.vert_policy === 'down')       posY = pos.top + parentHeight + 8;
                else if(this.props.vert_policy === 'down')      posY = pos.top + parentHeight;
                else if(this.props.vert_policy === 'center')    posY = pos.top + parentHeight - contentHeight / 2;
            }
            else if(this.props.anchor === 'center-left' || this.props.anchor === 'center-right')
            {
                if(this.props.vert_policy === 'up')
                {
                    posY = pos.top + parentHeight/2 - contentHeight;
                    posY += 15+1;
                }
                else if(this.props.vert_policy === 'down')
                {
                    posY = pos.top + parentHeight/2;
                    posY -= 15+1;
                }
                else if(this.props.vert_policy === 'center')
                {
                    posY = (pos.top - (contentHeight/2 - parentHeight/2));
                }
            }
        }
        else
        {
            console.log("Popup: Invalid policy");
        }
        this.setState({
            posX,
            posY,
        });
    }
    componentWillUnmount()
    {
        document.removeEventListener('click', this.handleWindowMouseClick);
    }
    handleClick = (event) =>
    {
        event.preventDefault();
        event.stopPropagation();
        event.nativeEvent.stopImmediatePropagation();

        if(this.props.onClose)
        {
            if(this.props.closeable)
                this.props.onClose(event);
        }
    }
    handleWindowMouseClick = (event) =>
    {
        event.preventDefault();
        event.stopPropagation();

        if(this.props.onClose)
        {
            this.props.onClose(event);
        }
    }
};
export default Popup;
