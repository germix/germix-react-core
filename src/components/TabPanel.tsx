import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class TabPanel extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="tab-panel">
    { this.props.children }
</div>
        );
    }
}
export default TabPanel;
