import React from 'react';

interface Props
{
    fullwidth? : boolean,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class PopupWrapper extends React.Component<Props,State>
{
    render()
    {
        let popupClass = 'popup-wrapper';

        if(this.props.fullwidth)
            popupClass += ' fullwidth';

        return (
            <div className={popupClass}>
                { this.props.children }
            </div>
        );
    }
};
export default PopupWrapper;
