import React, { PropsWithChildren } from 'react';
import { ColorTypes } from '../types';

interface Props extends PropsWithChildren<any>
{
    color? : ColorTypes,
    onClose?(),
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Alert extends React.Component<Props,State>
{
    render()
    {
        let className = ['alert'];
        
        if(this.props.color !== undefined)
        {
            className.push('alert-' + this.props.color);
        }
        if(this.props.onClose !== undefined)
        {
            className.push('alert-dismissible');
        }

        return (
<div className={className.join(' ')}>
    { this.props.children }
    { this.props.onClose !== undefined &&
        <button
            type="button"
            className="close"
            data-dismiss="alert"
            aria-label="Close"
            onClick={() =>
            {
                if(this.props.onClose)
                    this.props.onClose();
            }}
        >
            <span aria-hidden="true">&times;</span>
        </button>
    }
</div>
        );
    }
};
export default Alert;
