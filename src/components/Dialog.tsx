import React, { PropsWithChildren } from 'react';
import Modal from './Modal';

interface Props extends PropsWithChildren<any>
{
    open? : boolean,
    disableEscapeKeyDown? : boolean,
    disableBackdropClick? : boolean,
    onClose?(),
    onEscapeKeyDown?(event),
    onBackdropClick?(event),
    width?,
    height?,
}
interface State
{
}

/**
 * @author Dialog Martínez
 */
class Dialog extends React.Component<Props,State>
{
    render()
    {
        const style =
        {
            width: this.props.width,
            height: this.props.height,
        }
        return (
<Modal
    open={this.props.open}
    disableEscapeKeyDown={this.props.disableEscapeKeyDown}
    disableBackdropClick={this.props.disableBackdropClick}
    onClose={this.props.onClose}
    onEscapeKeyDown={this.props.onEscapeKeyDown}
    onBackdropClick={this.props.onBackdropClick}
    >
    <div className="dialog" style={style}>
        { this.props.children }
    </div>
</Modal>
        );
    }
};
export default Dialog;
