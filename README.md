# Germix React Core

## About

Germix react core components

## Installation

```bash
npm install @germix/germix-react-core
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
